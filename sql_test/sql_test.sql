/*
 * sql_test.sql
 * Copyright (C) 2020 Tolmachev V.N. <>
 *
 * Distributed under terms of the MIT license.
 * запрос для MySQL для получения месяца (числового значения) из времени на 20 дней позже значения из utime.
 */

select month(@a) where @a:=(select date_sub(@ldate, interval 20 day) where @ldate:=(select utime  from timetest order by utime desc limit 1 ));

