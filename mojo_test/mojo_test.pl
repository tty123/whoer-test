#!/usr/bin/env perl
use HTTP::BrowserDetect ();
use Mojolicious::Lite;
use Mojolicious::Plugin::RemoteAddr;


plugin('RemoteAddr');

get '/user/info' => sub {
  my $c=shift; 
  #---- данные для шаблона и JSON
  my $user_agent_str = $c->req->headers->user_agent; 
  my $accept = $c->req->headers->accept;
  my $ua = HTTP::BrowserDetect->new($user_agent_str);
  my $browser = $ua->browser_string;
  my $os = $ua->os_string;
  my $ip = $c->remote_addr;
  my %data = ( ip => $ip,
	       os => $os,
	       browser => $browser,
             );
  $c->stash(%data);
  #-----------------------------
  if ($accept=~/application\/json/){
	  $c->respond_to ( json => { json=> \%data} );
      }
      else {
          $c->render(template => 'index');
      }
};


app->start;
__DATA__

@@ index.html.ep
% layout 'default';
% title 'Данные подключения';
<br>
<b> IP: </b> <%=$ip%> <br>
<b> OS: </b> <%=$os%> <br>
<b> Browser: </b> <%=$browser%> <br>

@@ layouts/default.html.ep
<!DOCTYPE html>
<html>
  <head><title><%= title %></title></head>
  <body><%= content %></body>
</html>
